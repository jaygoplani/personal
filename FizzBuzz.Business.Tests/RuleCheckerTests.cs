﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FizzBuzz.Business.Tests
{
    [TestClass]
    public class RuleCheckerTests
    {
        [TestMethod]
        public void GetMatchedRule_WhenConditionMatch_ReturnsRule()
        {
            var ruleChecker = new RuleChecker();

            var rules = ruleChecker.GetMatchedRules(3);

            Assert.IsNotNull(rules);
        }

        [TestMethod]
        public void GetMatchedRule_WhenConditionMatchForBothRules_ReturnsMultipleRules()
        {
            var ruleChecker = new RuleChecker();

            var matchedRules = ruleChecker.GetMatchedRules(15);

            Assert.IsNotNull(matchedRules);
            Assert.AreEqual(matchedRules.Count, 2);
        }

        [TestMethod]
        public void GetMatchedRule_WhenConditionNotMatched_ReturnsEmptyRules()
        {
            var ruleChecker = new RuleChecker();

            var matchedRules = ruleChecker.GetMatchedRules(1);

            Assert.AreEqual(matchedRules.Count, 0);
        }
    }
}
