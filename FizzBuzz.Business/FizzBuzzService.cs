﻿using FizzBuzz.Business.Interface;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzz.Business
{
    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IRuleChecker _ruleChecker;

        public FizzBuzzService(IRuleChecker ruleChecker)
        {
            _ruleChecker = ruleChecker;
        }

        /// <summary>
        /// 
        /// This method is to get the final result string based on the matched rules 
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        public IList<string> GetRecordSet(int number)
        {
            var records = new List<string>();

            for (var index = 1; index <= number; index++)
            {
                var displayString = new StringBuilder();

                var rules = _ruleChecker.GetMatchedRules(index);

                if (rules != null && rules.Any())
                {
                    foreach (var rule in rules)
                    {
                        displayString.Append(rule.DisplayString());
                    }
                    records.Add(displayString.ToString());
                }
                else
                {
                    records.Add(index.ToString());
                }
            }
            return records;
        }
    }
}
