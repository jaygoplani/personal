﻿using FizzBuzz.Business.Interface;

namespace FizzBuzz.Business.Rules
{
    public class FizzRule : IFizzBuzzRule
    {
        private readonly IDayCheck _wizzWuzzRule;
        public FizzRule(IDayCheck wizzWuzzRule)
        {
            _wizzWuzzRule = wizzWuzzRule;
        }

        public string DisplayString()
        {
            return _wizzWuzzRule.IsWednesday() ? "Wizz" : "Fizz";
        }

        public bool IsMatched(int number)
        {
            return number % 3 == 0;
        }
    }
}
