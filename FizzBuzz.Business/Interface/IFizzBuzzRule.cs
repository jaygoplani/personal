﻿namespace FizzBuzz.Business.Interface
{
    public interface IFizzBuzzRule
    {
        bool IsMatched(int number);
        string DisplayString();
    }
}
