﻿using FizzBuzz.Business.Interface;
using System;

namespace FizzBuzz.Business
{
    public class DayCheck : IDayCheck
    {
        public bool IsWednesday()
        {
            return DateTime.Now.DayOfWeek == DayOfWeek.Wednesday;
        }
    }
}